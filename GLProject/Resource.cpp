#include "stdafx.h"
#include "Resource.h"

std::map<std::string, std::shared_ptr<Texture>> Resource::textures;
std::map<std::string, std::shared_ptr<Material>> Resource::materials;
std::map<std::string, std::shared_ptr<Shader>> Resource::shaders;
std::map<std::string, std::shared_ptr<Model>> Resource::models;
std::map<std::string, std::shared_ptr<Mesh>> Resource::meshes;
std::map<std::string, std::shared_ptr<Camera>> Resource::cameras;
std::map<std::string, std::shared_ptr<DirLight>> Resource::dirLights;
std::map<std::string, std::shared_ptr<PointLight>> Resource::pointLights;
std::map<std::string, std::shared_ptr<CubeMap>> Resource::cubeMaps;

Resource::Resource()
{
}

Resource::~Resource()
{
}

void Resource::CreateTexture(std::string name, std::string filePath)
{
	textures[name] = std::make_shared<Texture>(name, filePath);
}

void Resource::CreateMaterial(std::string name,std::string albedo, std::string normalMap, std::string roughMetalAO)
{
	std::vector<std::shared_ptr<Texture>> vec = {GetTexture(albedo), GetTexture(normalMap), GetTexture(roughMetalAO)};
	materials[name] = std::make_shared<Material>(name, vec);
}

void Resource::CreateShader(std::string name, std::string vertexFilePath, std::string fragmentFilePath)
{
	shaders[name] = std::make_shared<Shader>(name, vertexFilePath, fragmentFilePath);
}

void Resource::CreateModel(std::string name, std::string meshName, std::vector<std::string> materialNames)
{
	std::vector<std::shared_ptr<Material>> mat;
	for (int i = 0; i < materialNames.size(); i++)
	{
		mat.push_back(GetMaterial(materialNames.at(i)));
	}

	models[name] = std::make_shared<Model>(name, GetMesh(meshName), mat);
}

void Resource::CreateMesh(std::string name, std::string filePath)
{
	meshes[name] = std::make_shared<Mesh>(name, filePath);
}

void Resource::CreateCamera(std::string name, glm::vec3 position, float FOV)
{
	cameras[name] = std::make_shared<Camera>(name, position, FOV);
}

void Resource::CreatePointLight(std::string name, glm::vec3 position, glm::vec3 color, glm::vec3 constantLinearQuadratic)
{
	pointLights[name] = std::make_shared<PointLight>(position, color, constantLinearQuadratic);
}

void Resource::CreateDirLight(std::string name, glm::vec3 direction, glm::vec3 color)
{
	dirLights[name] = std::make_shared<DirLight>(direction, color);
}

void Resource::CreateCubeMap(std::string name, std::vector<std::string> filePaths)
{
	cubeMaps[name] = std::make_shared<CubeMap>(name, filePaths);
}

std::shared_ptr<Texture> Resource::GetTexture(std::string name)
{
	return textures[name];
}

std::shared_ptr<Material> Resource::GetMaterial(std::string name)
{
	return materials[name];
}

std::shared_ptr<Shader> Resource::GetShader(std::string name)
{
	return shaders[name];
}

std::shared_ptr<Model> Resource::GetModel(std::string name)
{
	return models[name];
}

std::shared_ptr<Mesh> Resource::GetMesh(std::string name)
{
	return meshes[name];
}

std::shared_ptr<Camera> Resource::GetCamera(std::string name)
{
	return cameras[name];
}

std::shared_ptr<DirLight> Resource::GetDirLight(std::string name)
{
	return dirLights[name];
}

std::shared_ptr<PointLight> Resource::GetPointLight(std::string name)
{
	return pointLights[name];
}

std::shared_ptr<CubeMap> Resource::GetCubeMap(std::string name)
{
	return cubeMaps[name];
}

std::vector<std::shared_ptr<Model>> Resource::GetAllModels()
{
	std::vector<std::shared_ptr<Model>> mod;
	for (const auto &i : models)
	{
		mod.push_back((i.second));
	}
	return mod;
}

std::vector<std::shared_ptr<PointLight>> Resource::GetAllPointLights()
{
	std::vector<std::shared_ptr<PointLight>> lights;
	for (const auto &i : pointLights)
	{
		lights.push_back(i.second);
	}
	return lights;
}

std::vector<std::shared_ptr<DirLight>> Resource::GetAllDirLights()
{
	std::vector<std::shared_ptr<DirLight>> lights;
	for (const auto &i : dirLights)
	{
		lights.push_back(i.second);
	}
	return lights;
}
