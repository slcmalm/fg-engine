#pragma once
#include <memory>
#include "ResourceObject.h"
#include "Texture.h"

class Material: public ResourceObject
{
public:
	Material(std::string name, std::vector<std::shared_ptr<Texture>> textures);
	Material();
	~Material();

	void Update(GLuint shaderProgram);

	void SetTextures(std::vector<std::shared_ptr<Texture>> textures);
	std::vector<std::shared_ptr<Texture>> GetTextures();

private:
	std::vector<std::shared_ptr<Texture>> textures;
};

