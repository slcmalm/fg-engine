#pragma once
#include "GL/glew.h"
#include "glm/glm.hpp"
#include "ResourceObject.h"

class Camera: ResourceObject
{
public:
	Camera(std::string name, glm::vec3 position, float FOV);
	Camera();
	~Camera();

	void Update();

	void RotateYaw(float degrees);
	void RotatePitch(float degrees);
	void Move(glm::vec3 axis, float amount);

	void SetFOV(float FOV);

	float GetFOV();

	glm::mat4 GetViewMatrix();
	glm::vec3 GetPosition();

private:
	float FOV, yaw = 0.0f;
	float pitch = 90.0f;

	glm::mat4 viewMatrix;
	glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 forwardVector = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 upVector = glm::vec3(0.0f, 1.0f, 0.0f);
	glm::vec3 rightVector = glm::vec3(1.0f, 0.0f, 0.0f);
};
