#include "stdafx.h"
#include "Renderer.h"
#include "Resource.h"
#include <memory>
#include <string>
#include "Model.h"
#include "Camera.h"

int main()
{
	Renderer renderer = Renderer("Sophie Malmberg - Futuregames 2018-04-30", false, 1920, 1080);
	
	Resource::CreateShader("LightingShader", "..\\Shaders\\PBR_DeferredLightVS.glsl", "..\\Shaders\\PBR_DeferredLightFS.glsl");
	Resource::CreateShader("GeometryShader", "..\\Shaders\\PBR_DeferredGeometryVS.glsl", "..\\Shaders\\PBR_DeferredGeometryFS.glsl");
	Resource::CreateShader("SkyBoxShader", "..\\Shaders\\SkyBoxVS.glsl", "..\\Shaders\\SkyBoxFS.glsl");
	
	std::vector<std::string> skyBoxPaths = { "../Textures/SkyBox/SkyBox04.png", "../Textures/SkyBox/SkyBox02.png",
		"../Textures/SkyBox/SkyBox05.png", "../Textures/SkyBox/SkyBox06.png", "../Textures/SkyBox/SkyBox03.png", "../Textures/SkyBox/SkyBox01.png" };

	Resource::CreateCubeMap("SkyBox", skyBoxPaths);
	Resource::CreateCamera("MainCamera", glm::vec3(0.0f, 0.0f, 45.0f), 75);
	Resource::CreateDirLight("DirLight", glm::vec3(-0.2f, -1.0f, -0.3f), glm::vec3(1.0f));
	Resource::CreatePointLight("PointLight", glm::vec3(3.0f, 0.0f, 20.0f), glm::vec3(0.0f, 1.0f, 1.0f), glm::vec3(1.0f, 0.89f, 0.032f));

	std::shared_ptr<Camera> camera = Resource::GetCamera("MainCamera");

	renderer.SetSkyBox(Resource::GetCubeMap("SkyBox"));
	renderer.SetCamera(Resource::GetCamera("MainCamera"));
	renderer.SetShaderGeometry(Resource::GetShader("GeometryShader"));
	renderer.SetShaderLight(Resource::GetShader("LightingShader"));
	renderer.SetShaderSkyBox(Resource::GetShader("SkyBoxShader"));

	float lastTime = 0;
	GLFWwindow *window = renderer.GetWindow();

	while (!glfwWindowShouldClose(window))
	{
		float currentTime = (float)glfwGetTime();
		float deltaTime = float(currentTime - lastTime);
		lastTime = currentTime;
		renderer.Update(deltaTime);
	}

    return 0;
}

