#include "stdafx.h"
#include "Shader.h"
#include <iostream>
#include <fstream>

Shader::Shader(std::string name, std::string vertexfilePath, std::string fragmentfilePath)
{
	this->SetName(name);
	this->AddFilePath(vertexfilePath);
	this->AddFilePath(fragmentfilePath);

	std::string vertexShaderCode = ReadShaderFromFile(vertexfilePath);
	std::string fragmentShaderCode = ReadShaderFromFile(fragmentfilePath);

	GLuint vertexShader = CreateShaderProgram(GL_VERTEX_SHADER, vertexShaderCode, vertexfilePath);
	GLuint fragmentShader = CreateShaderProgram(GL_FRAGMENT_SHADER, fragmentShaderCode, fragmentfilePath);
	GLuint program = glCreateProgram();

	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);
	GLint linkResult = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &linkResult);

	if (linkResult == 0)
	{
		int logLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
		std::vector<char> programLog(logLength);
		glGetProgramInfoLog(program, logLength, 0, &programLog[0]);
		std::cout << "Shader error: " << std::endl << &programLog[0] << std::endl;
	}

	this->program = program;
}

Shader::Shader()
{
}

Shader::~Shader()
{
}

void Shader::Update()
{
	glUseProgram(program);
}

GLuint Shader::GetProgram()
{
	return program;
}

std::string Shader::ReadShaderFromFile(std::string filePath)
{
	std::string shaderCode;
	std::ifstream file(filePath, std::ios::in);

	if (!file.good())
	{
		std::cout << "ERROR: Unable to read file: " << filePath << std::endl;
		terminate();
	}

	file.seekg(0, std::ios::end);
	shaderCode.resize((unsigned int)file.tellg());
	file.seekg(0, std::ios::beg);
	file.read(&shaderCode[0], shaderCode.size());
	file.close();

	return shaderCode;
}

GLuint Shader::CreateShaderProgram(GLenum shaderType, std::string source, std::string shaderName)
{
	unsigned int shader = glCreateShader(shaderType);
	const char *shaderCode = source.c_str();
	const int shaderCodeSize = source.size();

	glShaderSource(shader, 1, &shaderCode, &shaderCodeSize);
	glCompileShader(shader);

	int compileResult = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compileResult);

	if (compileResult == GL_FALSE)
	{
		int logLength = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
		std::vector<char> shaderLog(1000);
		glGetShaderInfoLog(shader, logLength, 0, &shaderLog[0]);
		std::cout << "Unable to compile shader: " << shaderName << std::endl << &shaderLog[0] << std::endl;

		return 0;
	}

	return shader;
}
