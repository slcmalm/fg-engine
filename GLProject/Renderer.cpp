#include "stdafx.h"
#include "Renderer.h"
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Resource.h"

Renderer::Renderer(std::string windowTitle, bool isResizable, GLuint windowWidth, GLuint windowHeight)
{
	this->SetWindowTitle(windowTitle);
	this->windowWidth = windowWidth;
	this->windowHeight = windowHeight;

	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, isResizable);
	glEnable(GL_FRAMEBUFFER_SRGB);

	glfwWindow = glfwCreateWindow(windowWidth, windowHeight, windowTitle.c_str(), 0, 0);
	if (!glfwWindow)
	{
		std::cout << "ERROR: Unable to create GLFW window.";
		glfwTerminate();
		return;
	}

	glfwMakeContextCurrent(glfwWindow);
	glewExperimental = GL_TRUE;

	if (glewInit() != GLEW_OK)
	{
		std::cout << "ERROR: Unable to initialize GLEW.";
		glfwTerminate();
		return;
	}

	glViewport(0, 0, windowWidth, windowHeight);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	gBuffer.Initialize(windowWidth, windowHeight);
}

Renderer::~Renderer()
{
}

void Renderer::Update(float deltaTime)
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	models = Resource::GetAllModels();
	RenderGeometryPass(geometryShader->GetProgram(), camera);
	RenderLightPass(lightShader->GetProgram(), camera);
	RenderSkyBox(skyBoxShader->GetProgram(), camera);
	glfwSwapBuffers(glfwWindow);
	glfwPollEvents();
}

void Renderer::SetWindowTitle(std::string title)
{
	this->windowTitle = title;
}

void Renderer::SetCamera(std::shared_ptr<Camera> camera)
{
	this->camera = camera;
}

void Renderer::SetShaderSkyBox(std::shared_ptr<Shader> shader)
{
	this->skyBoxShader = shader;
}

void Renderer::SetShaderGeometry(std::shared_ptr<Shader> shader)
{
	this->geometryShader = shader;
}

void Renderer::SetShaderLight(std::shared_ptr<Shader> shader)
{
	this->lightShader = shader;
}

void Renderer::SetSkyBox(std::shared_ptr<CubeMap> skyBox)
{
	this->skyBox = skyBox;
}

GLFWwindow *Renderer::GetWindow()
{
	return glfwWindow;
}

void Renderer::RenderGeometryPass(GLuint shaderProgram, std::shared_ptr<Camera> camera)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBindFramebuffer(GL_FRAMEBUFFER, gBuffer.GetBuffer());
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(shaderProgram);
	SetCameraMatrices(shaderProgram, camera);

	for (int i = 0; i < models.size(); i++)
	{
		for (int j = 0; j < models.at(i)->GetMaterials().size(); j++)
		{
			models.at(i)->GetMaterials().at(j)->Update(shaderProgram);
			models.at(i)->Update(shaderProgram);
		}
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::RenderLightPass(GLuint shaderProgram, std::shared_ptr<Camera> camera)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(shaderProgram);

	int viewSource = glGetUniformLocation(shaderProgram, "viewPosition");
	glUniform3fv(viewSource, 1, glm::value_ptr(camera->GetPosition()));

	pointLights = Resource::GetAllPointLights();
	dirLights = Resource::GetAllDirLights();

	for (int i = 0; i < dirLights.size(); i++)
	{
		dirLights.at(i)->Update(shaderProgram);
	}

	for (int i = 0; i < pointLights.size(); i++)
	{
		pointLights.at(i)->Update(shaderProgram, i);
	}

	gBuffer.Update(shaderProgram);
	glBlitFramebuffer(0, 0, windowWidth, windowHeight, 0, 0, windowWidth, windowHeight, GL_DEPTH_BUFFER_BIT, GL_NEAREST);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::RenderSkyBox(GLuint shaderProgram, std::shared_ptr<Camera> camera)
{
	glDepthFunc(GL_LEQUAL);
	glUseProgram(shaderProgram);
	glm::mat4 view = glm::mat4(glm::mat3(camera->GetViewMatrix()));
	int skyVSource = glGetUniformLocation(shaderProgram, "view");
	glUniformMatrix4fv(skyVSource, 1, GL_FALSE, glm::value_ptr(view));

	glm::mat4 projection = glm::perspective(glm::radians(camera->GetFOV()), (float)windowWidth/(float)windowHeight, 0.f, 1000.0f);
	int skyPSource = glGetUniformLocation(shaderProgram, "projection");
	glUniformMatrix4fv(skyPSource, 1, GL_FALSE, glm::value_ptr(projection));
	
	glUniform1i(glGetUniformLocation(shaderProgram, "skyBox"), 0);
	skyBox->Update();
	glDepthFunc(GL_LESS);
}

void Renderer::SetCameraMatrices(GLuint shaderProgram, std::shared_ptr<Camera> camera)
{
	glm::mat4 view = camera->GetViewMatrix();
	glm::mat4 projection = glm::perspective(glm::radians(camera->GetFOV()), (float)windowWidth/ (float)windowHeight, 0.01f, 1000.0f);
	glm::mat4 viewProjection = projection * view;
	int VPSource = glGetUniformLocation(shaderProgram, "viewProjection");
	glUniformMatrix4fv(VPSource, 1, GL_FALSE, glm::value_ptr(viewProjection));
}
