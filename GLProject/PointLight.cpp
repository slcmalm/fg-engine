#include "stdafx.h"
#include "PointLight.h"
#include <glm/gtc/type_ptr.hpp>
#include <string>

PointLight::PointLight(glm::vec3 position, glm::vec3 color, glm::vec3 constantLinearQuadratic)
{
	this->SetPosition(position);
	this->SetColor(color);
	this->SetCLQ(constantLinearQuadratic);
}

PointLight::PointLight()
{
}


PointLight::~PointLight()
{
}

void PointLight::Update(GLuint shaderProgram, GLuint index)
{
	std::string number = std::to_string(index);

	int lightPositionSource = glGetUniformLocation(shaderProgram, ("pointLights[" + number + "].position").c_str());
	glUniform3fv(lightPositionSource, 1, glm::value_ptr(position));
	int linearSource = glGetUniformLocation(shaderProgram, ("pointLights[" + number + "].linear").c_str());
	glUniform1fv(linearSource, 1, &linear);
	int constantSource = glGetUniformLocation(shaderProgram, ("pointLights[" + number + "].constant").c_str());
	glUniform1fv(constantSource, 1, &constant);
	int quadraticSource = glGetUniformLocation(shaderProgram, ("pointLights[" + number + "].quadratic").c_str());
	glUniform1fv(quadraticSource, 1, &quadratic);
	int lightColorSource = glGetUniformLocation(shaderProgram, ("pointLights[" + number + "].color").c_str());
	glUniform3fv(lightColorSource, 1, glm::value_ptr(color));
}

void PointLight::SetPosition(glm::vec3 position)
{
	this->position = position;
}

void PointLight::SetColor(glm::vec3 color)
{
	this->color = color;
}

void PointLight::SetCLQ(glm::vec3 constantLightQuadratic)
{
	this->constantLinearQuadratic = constantLightQuadratic;
}

glm::vec3 PointLight::GetPosition()
{
	return position;
}

glm::vec3 PointLight::GetColor()
{
	return color;
}

glm::vec3 PointLight::GetCLQ()
{
	return constantLinearQuadratic;
}

