#include "stdafx.h"
#include "Camera.h"
#include <glm/gtc/matrix_transform.hpp>

Camera::Camera(std::string name, glm::vec3 position, float FOV)
{
	this->SetName(name);
	this->FOV = FOV;
	this->position = position;
	
}


Camera::Camera()
{
}


Camera::~Camera()
{
}

void Camera::Update()
{
	glm::vec3 forward;
	forward.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	forward.y = sin(glm::radians(pitch));
	forward.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));

	forwardVector = glm::normalize(forward);
}

void Camera::RotateYaw(float degrees)
{
	yaw += degrees;
}

void Camera::RotatePitch(float degrees)
{
	pitch += degrees;

	if (pitch > 89.0f)
	{
		pitch = 89.0f;
	}
	else if (pitch < -89.0f)
	{
		pitch = -89.0f;
	}
}

void Camera::Move(glm::vec3 axis, float amount)
{
	glm::vec3 movement = axis * amount;
	position += movement;
}

void Camera::SetFOV(float FOV)
{
	this->FOV = FOV;
}

float Camera::GetFOV()
{
	return FOV;
}

glm::mat4 Camera::GetViewMatrix()
{
	viewMatrix = glm::lookAt(position, position + forwardVector, upVector);
	return viewMatrix;
}

glm::vec3 Camera::GetPosition()
{
	return position;
}
