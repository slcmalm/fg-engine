#include "stdafx.h"
#include "Material.h"

Material::Material(std::string name, std::vector<std::shared_ptr<Texture>> textures)
{
	this->SetName(name);
	this->textures = textures;
}

Material::Material()
{
}

Material::~Material()
{
}

void Material::Update(GLuint shaderProgram)
{
	glUniform1i(glGetUniformLocation(shaderProgram, (char*)"material.albedoMap"), 0);
	glUniform1i(glGetUniformLocation(shaderProgram, (char*)"material.normalMap"), 1);
	glUniform1i(glGetUniformLocation(shaderProgram, (char*)"material.roughMetalAO"), 2);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textures.at(0)->GetTexture());

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, textures.at(1)->GetTexture());

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, textures.at(2)->GetTexture());
}

void Material::SetTextures(std::vector<std::shared_ptr<Texture>> textures)
{
	this->textures = textures;
}

std::vector<std::shared_ptr<Texture>> Material::GetTextures()
{
	return textures;
}
