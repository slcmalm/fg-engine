#pragma once
#include "GL/glew.h"
#include "ResourceObject.h"
#include <string>

class Shader: public ResourceObject
{
public:
	Shader(std::string name, std::string vertexfilePath, std::string fragmentfilePath);
	Shader();
	~Shader();

	virtual void Update();
	GLuint GetProgram();

private:
	GLuint program;

	std::string ReadShaderFromFile(std::string filePath);
	GLuint CreateShaderProgram(GLenum shaderType, std::string source, std::string shaderName);
};

