#pragma once
#include <map>
#include <memory>
#include "glm/glm.hpp"
#include "Shader.h"
#include "Model.h"
#include "Mesh.h"
#include "Material.h"
#include "Texture.h"
#include "Camera.h"
#include "DirLight.h"
#include "PointLight.h"
#include "CubeMap.h"

class Resource
{
public:
	static void CreateTexture(std::string name, std::string filePath);
	static void CreateMaterial(std::string name, std::string albedo, std::string normalMap, std::string roughMetalAO);
	static void CreateShader(std::string name, std::string vertexFilePath, std::string fragmentFilePath);
	static void CreateModel(std::string name, std::string meshName, std::vector<std::string> materialNames);
	static void CreateMesh(std::string name, std::string filePath);
	static void CreateCamera(std::string name, glm::vec3 position, float FOV);
	static void CreatePointLight(std::string name, glm::vec3 position, glm::vec3 color, glm::vec3 constantLinearQuadratic);
	static void CreateDirLight(std::string name, glm::vec3 direction, glm::vec3 color);
	static void CreateCubeMap(std::string name, std::vector<std::string> filePaths);

	static std::shared_ptr<Texture> GetTexture(std::string name);
	static std::shared_ptr<Material> GetMaterial(std::string name);
	static std::shared_ptr<Shader> GetShader(std::string name);
	static std::shared_ptr<Model> GetModel(std::string name);
	static std::shared_ptr<Mesh> GetMesh(std::string name);
	static std::shared_ptr<Camera> GetCamera(std::string name);
	static std::shared_ptr<DirLight> GetDirLight(std::string name);
	static std::shared_ptr<PointLight> GetPointLight(std::string name);
	static std::shared_ptr<CubeMap> GetCubeMap(std::string name);

	static std::vector<std::shared_ptr<Model>> GetAllModels();
	static std::vector<std::shared_ptr<PointLight>> GetAllPointLights();
	static std::vector<std::shared_ptr<DirLight>> GetAllDirLights();

private:
	static std::map<std::string, std::shared_ptr<Texture>> textures;
	static std::map<std::string, std::shared_ptr<Material>> materials;
	static std::map<std::string, std::shared_ptr<Shader>> shaders;
	static std::map<std::string, std::shared_ptr<Model>> models;
	static std::map<std::string, std::shared_ptr<Mesh>> meshes;
	static std::map<std::string, std::shared_ptr<Camera>> cameras;
	static std::map<std::string, std::shared_ptr<DirLight>> dirLights;
	static std::map<std::string, std::shared_ptr<PointLight>> pointLights;
	static std::map<std::string, std::shared_ptr<CubeMap>> cubeMaps;

	Resource();
	~Resource();
};

