#pragma once
#include <vector>
#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/mesh.h"
#include "ResourceObject.h"

class Mesh: public ResourceObject
{
public:
	Mesh(std::string name, std::string filePath);
	Mesh();
	~Mesh();

	void Update();

private:
	struct MeshNode
	{
		MeshNode(aiMesh *mesh);
		~MeshNode();

		unsigned int vertexArray;
		unsigned int elementCount;
		unsigned int vertexBuffer, textureBuffer, indexBuffer, normalBuffer, tangentBuffer = 0;

		void Update();
	};

	const aiScene *scene;
	std::vector<MeshNode*> meshNodes;
};

