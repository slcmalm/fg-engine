#pragma once
#include "GL/glew.h"
#include "glm/glm.hpp"

struct PointLight
{
public:
	PointLight(glm::vec3 position, glm::vec3 color, glm::vec3 constantLinearQuadratic);
	PointLight();
	~PointLight();

	void Update(GLuint shaderProgram, GLuint index);
	void SetPosition(glm::vec3 position);
	void SetColor(glm::vec3 color);
	void SetCLQ(glm::vec3 constantLightQuadratic);

	glm::vec3 GetPosition();
	glm::vec3 GetColor();
	glm::vec3 GetCLQ();

private:
	float constant = 1.0f;
	float linear = 0.45f;
	float quadratic = 0.075f;
	
	glm::vec3 position, color, constantLinearQuadratic;
};

