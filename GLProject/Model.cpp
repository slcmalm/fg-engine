#include "stdafx.h"
#include "Model.h"
#include <glm/gtc/type_ptr.hpp>

Model::Model(std::string name, std::shared_ptr<Mesh> mesh, std::vector<std::shared_ptr<Material>> materials)
{
	this->SetName(name);
	this->SetMesh(mesh);
	this->SetMaterials(materials);
}

Model::Model()
{
}

Model::~Model()
{
}

void Model::Update(GLuint shaderProgram)
{
	unsigned int transformLocation = glGetUniformLocation(shaderProgram, "meshTransform");
	glUniformMatrix4fv(transformLocation, 1, GL_FALSE, glm::value_ptr(transform));
	mesh->Update();
}

void Model::SetMesh(std::shared_ptr<Mesh> mesh)
{
	this->mesh = mesh;
}

void Model::SetMaterials(std::vector<std::shared_ptr<Material>> materials)
{
	this->materials = materials;
}

std::vector<std::shared_ptr<Material>> Model::GetMaterials()
{
	return materials;
}
