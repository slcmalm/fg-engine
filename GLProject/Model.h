#pragma once
#include <memory>
#include <string>
#include "GameObject.h"
#include "Mesh.h"
#include "Material.h"

class Model: public GameObject
{
public:
	Model(std::string name, std::shared_ptr<Mesh> mesh, std::vector<std::shared_ptr<Material>> materials);
	Model();
	~Model();

	void Update(GLuint shaderProgram);

	void SetMesh(std::shared_ptr<Mesh> mesh);
	void SetMaterials(std::vector<std::shared_ptr<Material>> materials);

	std::vector<std::shared_ptr<Material>> GetMaterials();

private:
	std::shared_ptr<Mesh> mesh;
	std::vector<std::shared_ptr<Material>> materials;
};

