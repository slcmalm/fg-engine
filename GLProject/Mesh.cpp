#include "stdafx.h"
#include "Mesh.h"
#include "GL/glew.h"
#include <iostream>
#include "assimp/postprocess.h"

Mesh::Mesh(std::string name, std::string filePath)
{
	this->AddFilePath(filePath);
	this->SetName(name);

	Assimp::Importer importer;
	scene = importer.ReadFile(filePath, aiProcessPreset_TargetRealtime_Fast | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
	if (!scene)
	{
		std::cout << "Model error: " << importer.GetErrorString();
		return;
	}

	for (int i = 0; i < scene->mNumMeshes; i++)
	{
		meshNodes.push_back(new Mesh::MeshNode(scene->mMeshes[i]));
	}
}

Mesh::Mesh()
{
}

void Mesh::Update()
{
	for (int i = 0; i < meshNodes.size(); i++)
	{
		meshNodes.at(i)->Update();
	}
}


Mesh::~Mesh()
{

}

Mesh::MeshNode::MeshNode(aiMesh *mesh)
{
	glGenVertexArrays(1, &vertexArray);
	glBindVertexArray(vertexArray);

	elementCount = mesh->mNumFaces * 3;

	if (mesh->HasPositions())
	{
		std::vector<float> vertices;

		for (int i = 0; i < mesh->mNumVertices; i++)
		{
			vertices.push_back(mesh->mVertices[i].x);
			vertices.push_back(mesh->mVertices[i].y);
			vertices.push_back(mesh->mVertices[i].z);
		}

		glGenBuffers(1, &vertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), &vertices[0], GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(0);
	}

	if (mesh->HasFaces())
	{
		std::vector<unsigned int> indices;

		for (int i = 0; i < mesh->mNumFaces; i++)
		{
			indices.push_back(mesh->mFaces[i].mIndices[0]);
			indices.push_back(mesh->mFaces[i].mIndices[1]);
			indices.push_back(mesh->mFaces[i].mIndices[2]);
		}

		glGenBuffers(1, &indexBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(1);
	}


	if (mesh->HasTextureCoords(0))
	{
		std::vector<float> texCoords;
		for (int i = 0; i < mesh->mNumVertices; i++)
		{
			texCoords.push_back(mesh->mTextureCoords[0][i].x);
			texCoords.push_back(mesh->mTextureCoords[0][i].y);
		}
		glGenBuffers(1, &textureBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, textureBuffer);
		glBufferData(GL_ARRAY_BUFFER, texCoords.size() * sizeof(float), &texCoords[0], GL_STATIC_DRAW);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(2);
	}


	if (mesh->HasNormals())
	{
		std::vector<float> normals;

		for (int i = 0; i < mesh->mNumVertices; i++)
		{
			normals.push_back(mesh->mNormals[i].x);
			normals.push_back(mesh->mNormals[i].y);
			normals.push_back(mesh->mNormals[i].z);
		}
		glGenBuffers(1, &normalBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
		glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(float), &normals[0], GL_STATIC_DRAW);

		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(3);
	}

	if (mesh->HasTangentsAndBitangents())
	{
		std::vector<float> tangents;

		for (int i = 0; i < mesh->mNumVertices; i++)
		{
			tangents.push_back(mesh->mTangents[i].x);
			tangents.push_back(mesh->mTangents[i].y);
			tangents.push_back(mesh->mTangents[i].z);
		}

		glGenBuffers(1, &tangentBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, tangentBuffer);
		glBufferData(GL_ARRAY_BUFFER, tangents.size() * sizeof(float), &tangents[0], GL_STATIC_DRAW);

		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(4);
	}

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Mesh::MeshNode::Update()
{
	glBindVertexArray(vertexArray);
	glBindBuffer(GL_ARRAY_BUFFER, textureBuffer);
	glDrawElements(GL_TRIANGLES, elementCount, GL_UNSIGNED_INT, NULL);
	glBindVertexArray(0);
}


Mesh::MeshNode::~MeshNode()
{
}
