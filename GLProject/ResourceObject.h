#pragma once
#include <string>
#include <vector>

class ResourceObject
{
public:
	ResourceObject();
	~ResourceObject();

	virtual void Update();
	
	void SetName(std::string name);
	void SetFilePaths(std::vector<std::string> filePaths);
	void AddFilePath(std::string filePath);

	std::string GetName();

protected:
	std::string name;
	std::vector<std::string> filePaths = {};
};

