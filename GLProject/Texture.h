#pragma once
#include "ResourceObject.h"
#include <string>
#include "GL/glew.h"

class Texture: public ResourceObject
{
public:
	Texture(std::string name, std::string filePath);
	Texture();

	~Texture();

	const GLuint &GetTexture();

private:
	GLint  width, height;
	GLuint texture;
};

