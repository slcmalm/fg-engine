#include "stdafx.h"
#include "GameObject.h"
#include "glm/gtc/matrix_transform.hpp"

GameObject::GameObject()
{
}


GameObject::~GameObject()
{
}

void GameObject::Update()
{

}

void GameObject::SetName(std::string name)
{
	this->name = name;
}

void GameObject::SetPosition(glm::vec3 position)
{
	transform = glm::translate(transform, position); 
}

void GameObject::Scale(glm::vec3 size)
{
	transform = glm::scale(transform, size); ;
}

void GameObject::Rotate(glm::vec3 axis, float degrees)
{
	transform = glm::rotate(transform, glm::radians(degrees), axis);
}

glm::mat4 GameObject::GetTransform()
{
	return transform;
}

std::string GameObject::GetName()
{
	return name;
}
