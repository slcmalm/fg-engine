#include "stdafx.h"
#include "ResourceObject.h"


ResourceObject::ResourceObject()
{
}


ResourceObject::~ResourceObject()
{
}

void ResourceObject::Update()
{
}

std::string ResourceObject::GetName()
{
	return name;
}

void ResourceObject::SetName(std::string name)
{
	this->name = name;
}

void ResourceObject::SetFilePaths(std::vector<std::string> filePaths)
{
	this->filePaths = filePaths;
}

void ResourceObject::AddFilePath(std::string filePath)
{
	filePaths.push_back(filePath);
}
