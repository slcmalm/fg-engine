#pragma once
#include "GL/glew.h"
#include "glm/glm.hpp"

struct DirLight
{
public:
	DirLight(glm::vec3 direction, glm::vec3 color);
	DirLight();
	~DirLight();

	void Update(GLuint shaderProgram);

	void SetDirection(glm::vec3 direction);
	void SetColor(glm::vec3 color);

	glm::vec3 GetDirection();
	glm::vec3 GetColor();

private:
	glm::vec3 direction, color;
};

