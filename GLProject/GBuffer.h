#pragma once
#include "GL/glew.h"

class GBuffer
{
public:
	GBuffer();
	~GBuffer();

	void Initialize(GLint windowWidth, GLint windowHeight);
	void Update(GLuint program);

	const GLuint GetBuffer();

private:
	GLuint gBuffer, depthBuffer;
	GLuint gPositionTex, gNormalTex, gDiffSpecTex, gAlbedoTex, gRoughMetalAOTex;
	GLuint planeVBO, planeVAO;

	float planeVertices[20] =
	{
		-1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
		-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
		1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
	};
};

