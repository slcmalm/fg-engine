#include "stdafx.h"
#include "DirLight.h"
#include <glm/gtc/type_ptr.hpp>

DirLight::DirLight(glm::vec3 direction, glm::vec3 color)
{
	this->direction = direction;
	this->color = color;
}

DirLight::DirLight()
{
	this->direction = glm::vec3(0.0f);
	this->color = glm::vec3(1.0f);
}

DirLight::~DirLight()
{
	
}

void DirLight::Update(GLuint shaderProgram)
{
	int lightDirectionSource = glGetUniformLocation(shaderProgram, "dirLight.direction");
	glUniform3fv(lightDirectionSource, 1, glm::value_ptr(direction));
	int lightDiffuseSource = glGetUniformLocation(shaderProgram, "dirLight.color");
	glUniform3fv(lightDiffuseSource, 1, glm::value_ptr(color));
}

void DirLight::SetDirection(glm::vec3 direction)
{
	this->direction = direction;
}

void DirLight::SetColor(glm::vec3 color)
{
	this->color = color;
}

glm::vec3 DirLight::GetDirection()
{
	return direction;
}

glm::vec3 DirLight::GetColor()
{
	return color;
}
