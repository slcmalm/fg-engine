#pragma once
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include <string>
#include <vector>
#include <memory>
#include <map>
#include "GBuffer.h"
#include "Shader.h"
#include "Model.h"
#include "Camera.h"
#include "CubeMap.h"
#include "DirLight.h"
#include "PointLight.h"

class Renderer
{
public:
	Renderer(std::string windowTitle, bool isResizable, GLuint windowWidth, GLuint windowHeight);
	~Renderer();

	void Update(float deltaTime);

	void SetWindowTitle(std::string title);
	void SetCamera(std::shared_ptr<Camera> camera);
	void SetShaderSkyBox(std::shared_ptr<Shader> shader);
	void SetShaderGeometry(std::shared_ptr<Shader> shader);
	void SetShaderLight(std::shared_ptr<Shader> shader);
	void SetSkyBox(std::shared_ptr<CubeMap> skyBox);
	
	GLFWwindow *GetWindow();

private:
	void RenderGeometryPass(GLuint shaderProgram, std::shared_ptr<Camera> camera);
	void RenderLightPass(GLuint shaderProgram, std::shared_ptr<Camera> camera);
	void RenderSkyBox(GLuint shaderProgram, std::shared_ptr<Camera> camera);
	void SetCameraMatrices(GLuint shaderProgram, std::shared_ptr<Camera> camera);

	GLFWwindow *glfwWindow;
	std::string windowTitle = "";
	GBuffer gBuffer;
	GLuint windowWidth, windowHeight;
	GLuint currentShaderProgram;

	std::shared_ptr<Shader> geometryShader, lightShader, skyBoxShader;
	std::vector<std::shared_ptr<Model>> models;
	std::vector<std::shared_ptr<DirLight>> dirLights;
	std::vector<std::shared_ptr<PointLight>> pointLights;

	std::shared_ptr<Camera> camera;
	std::shared_ptr<CubeMap> skyBox;
};

