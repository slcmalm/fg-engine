#pragma once
#include "glm/glm.hpp"
#include <string>

class GameObject
{
public:
	GameObject();
	~GameObject();

	virtual void Update();

	void SetName(std::string name);
	void SetPosition(glm::vec3 position);
	void Scale(glm::vec3 size);
	void Rotate(glm::vec3 axis, float degrees);

	glm::mat4 GetTransform();
	std::string GetName();

protected:
	glm::mat4 transform = glm::mat4(1.0f);
	std::string name;
};

