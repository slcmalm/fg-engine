#version 330 core
layout (location = 0) out vec3 gPositionTex;
layout (location = 1) out vec3 gNormalTex;
layout (location = 2) out vec3 gAlbedoTex;
layout (location = 3) out vec3 gRoughMetalAOTex;

in vec2 texCoordinates;
in vec3 fragPosition;
in vec3 normalVector;

struct Material 
{
 sampler2D albedoMap;
 sampler2D normalMap;
 sampler2D roughMetalAO;
};

uniform Material material;

void main()
{
	vec3 normal = texture(material.normalMap, texCoordinates).rgb * normalVector;
	normal = normalize(normal * 2.0 - 1.0); 
	gNormalTex = normal;
	
	gAlbedoTex = texture(material.albedoMap, texCoordinates).rgb;
	gRoughMetalAOTex = texture(material.roughMetalAO, texCoordinates).rgb;
	
	gPositionTex = fragPosition;
}