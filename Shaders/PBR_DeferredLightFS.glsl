#version 330 core
in vec2 texCoordinates;

out vec4 color;

uniform sampler2D gPositionTex;
uniform sampler2D gNormalTex;
uniform sampler2D gAlbedoTex;
uniform sampler2D gRoughMetalAOTex;

struct PointLight 
{    
 vec3 position;
 vec3 color;
 
 float constant;
 float linear;
 float quadratic;  
}; 

struct DirLight 
{
 vec3 direction;
 vec3 color;
};

float DistributionGGX(vec3 N, vec3 H, float roughness);
float GeometrySchlickGGX(float NdotV, float roughness);
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness);
vec3 FresnelSchlick(float cosTheta, vec3 F0);

void CalculatePointLight(PointLight light);

#define NR_POINT_LIGHTS 1
uniform PointLight pointLights[NR_POINT_LIGHTS];
uniform DirLight dirLight;

uniform vec3 viewPosition;

const float pi = 3.1415926536;

void main()
{
	vec3 fragPosition = texture(gPositionTex, texCoordinates).rgb;
	vec3 normal = texture(gNormalTex, texCoordinates).rgb;
	vec3 albedo = texture(gAlbedoTex, texCoordinates).rgb;
	float AO = texture(gRoughMetalAOTex, texCoordinates).r;
	float roughness = texture(gRoughMetalAOTex, texCoordinates).g;
	float metallic = texture(gRoughMetalAOTex, texCoordinates).b;

	vec3 viewDirection = normalize(viewPosition - fragPosition);
	vec3 F0 = vec3(0.04); 
	F0 = mix(F0, albedo, metallic);
	
	vec3 Lo = vec3(0.0);

	for(int i = 0; i < NR_POINT_LIGHTS; i++)
	{
		vec3 lightDirection = normalize(pointLights[i].position - fragPosition);
		vec3 halfWayDir = normalize(viewDirection + lightDirection);
		float distance = length(pointLights[i].position - fragPosition);
		
		float attenuation = 1.0 / (pointLights[i].constant + pointLights[i].linear * distance + pointLights[i].quadratic * (distance * distance));    
		vec3 radiance = pointLights[i].color * attenuation;
		float NDF = DistributionGGX(normal, halfWayDir, roughness);   
		float G = GeometrySmith(normal, viewDirection, lightDirection, roughness);      
		vec3 F = fresnelSchlick(max(dot(halfWayDir, viewDirection), 0.0), F0);
           
		vec3 nominator = NDF * G * F; 
		float denominator = 4 * max(dot(normal, viewDirection), 0.0) * max(dot(normal, lightDirection), 0.0) + 0.001;
		vec3 specular = nominator / denominator;
		vec3 kS = F;
		vec3 kD = vec3(1.0) - kS;
		kD *= 1.0 - metallic;	  
		float NdotL = max(dot(normal, lightDirection), 0.0);  
		Lo += (kD * albedo / pi + specular) * radiance * NdotL;		
	}
	
	vec3 lightDirection = normalize(dirLight.direction - fragPosition);
	vec3 halfWayDir = normalize(viewDirection + lightDirection);
	vec3 radiance = dirLight.color;
	float NDF = DistributionGGX(normal, halfWayDir, roughness);   
	float G = GeometrySmith(normal, viewDirection, lightDirection, roughness);      
	vec3 F = fresnelSchlick(max(dot(halfWayDir, viewDirection), 0.0), F0);
	vec3 nominator = NDF * G * F; 
	float denominator = 4 * max(dot(normal, viewDirection), 0.0) * max(dot(normal, lightDirection), 0.0) + 0.001;
	vec3 specular = nominator / denominator;
	vec3 kS = F;
	vec3 kD = vec3(1.0) - kS;
	kD *= 1.0 - metallic;	  
	float NdotL = max(dot(normal, lightDirection), 0.0);  
	Lo += (kD * albedo / pi + specular) * radiance * NdotL;		
	
	vec3 diffuse = albedo * AO;
	vec3 diffuseLo = diffuse + Lo;
	color = vec4(diffuseLo, 1.0);	
}

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
	float a = roughness*roughness;
	float a2 = a*a;
	float NdotH = max(dot(N, H), 0.0);
	float NdotH2 = NdotH*NdotH;

	float nom = a2;
	float denom = (NdotH2 * (a2 - 1.0) + 1.0);
	denom = pi * denom * denom;

	return nom/denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
	float r = (roughness + 1.0);
	float k = (r*r)/8.0;

	float nom = NdotV;
	float denom = NdotV * (1.0 - k) + k;

	return nom/denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
	float NdotV = max(dot(N, V), 0.0);
	float NdotL = max(dot(N, L), 0.0);
	float ggx2 = GeometrySchlickGGX(NdotV, roughness);
	float ggx1 = GeometrySchlickGGX(NdotL, roughness);

	return ggx1 * ggx2;
}

vec3 FresnelSchlick(float cosTheta, vec3 F0)
{
	return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}
