#version 330 core
out vec4 color;

in vec3 texCoords;

uniform samplerCube skyBox;

void main()
{    
    color = texture(skyBox, texCoords);
}