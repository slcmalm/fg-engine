#version 330 core
layout (location = 0) in vec3 position;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in vec3 normal;

out vec2 texCoordinates;
out vec3 fragPosition;
out vec3 normalVector;

uniform mat4 viewProjection;
uniform mat4 meshTransform;

void main()
{
	gl_Position = viewProjection * meshTransform * vec4(position, 1.0f);
	
	fragPosition = vec3(meshTransform * vec4(position, 1.0));   
	mat3 normalMatrix = transpose(inverse(mat3(meshTransform)));
	normalVector = normalMatrix * normal;
	texCoordinates = texCoords;
}